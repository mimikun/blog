+++
title = "購読型サービスのまとめ"
date = "2018-09-30T08:37:24+09:00"
draft = false
description = ""
tags = []
categories = []
images = []
banner = ""
menu = ""
+++

FF内の人がやってるのを見て俺もやってみようとなりました。

今回は最初なので契約してるサービスだけで。

## 契約してるサービス
- ConoHa 1GB 972円/月 x2 (1944円)
    - Mastodonのメイン鯖とdev鯖で使用。
    - やっすいVPSにdev鯖を移せばもっと安くなるんかなあと思いつつまだできてない。

- AWS (従量課金なので不明) 約300円/月
    - Amazon S3でいくらか使っている

- note ニンジャスレイヤープラス 490円/月
    - ニンジャスレイヤーが好きなので契約してる。
    - あとdonation目的。記事更新メール届いたらすぐ見るレベル

- Adobe Creative Cloud コンプリートプラン 2138円/月
    - ぶっちゃけぜんぜんつかってない。(金の無駄

- オンゲキNET 324円/月
    - オンゲキNETのベーシックプラン。でも全然見てない (そもそもオンゲキそんなにプレイしてない

- iCloud 200GBプラン 400円/月
    - Macのストレージがキツキツなので契約してる

- Netflix 864円/月
    - 字幕付きでアニメが見たいのでNetflixを契約。(字幕がないと内容ぜんぜんわからんので仕方ない

- メインのドメイン 2548円/年
    - 安いところもしくはRoute53に移したいけどwhois guard使えるのかわからずできてない

## まとめ
月の支払合計は **約6700円** だった。

## 所感
オンゲキNETとAdobeは解約してもいいかもなあ, と思った。
