+++
title = "MastodonのEmojiを作るために、画像の背景を透過する方法"
date = "2018-06-30T23:02:32+09:00"
draft = false
description = ""
tags = []
categories = []
images = []
banner = ""
menu = ""
+++

MastodonのEmojiを作るのに、背景透過された画像が必要になったので作成しました。その時のメモを兼ねたものとなります。

InkscapeやGIMPでも良かったのですが、Photoshopの方が比較的使い慣れているのでPhotoshopで行うことにしました。

## 手順

### 1. Photoshopを起動し、Emojiにしたい画像を開く
今回は私のアイコンであるこの画像を選びました。

{{< figure src="https://s3-ap-northeast-1.amazonaws.com/mstdn.mimikun.jp/accounts/avatars/000/000/001/original/aff5ddc899df12a97404792086536182.png" class="center" width="320" height="320" >}}

### 2. 消しゴムツールを選択し、透明にしたい色を選択する
消しゴムツールを使うと、背景を透明にすることができます。

細かいところは消しゴムツールで消して、色が固まっているとか、同じ色であるだとか、そういうところを消したければ **マジック消しゴムツール** で一気に消すのが良さそうです。

終わったら、一旦保存します。~~データが消えると辛いので…~~

### 3. PNGで画像を保存
ファイル -> 書き出し -> 書き出し形式 と進みます。

Mastodonでは、画像ファイルが50KB以下でないとエラーになります。


なので、画像ファイルが50KB以下になっているか見ます。

{{< figure src="https://s3-ap-northeast-1.amazonaws.com/mstdn.mimikun.jp/media_attachments/files/000/124/427/original/222b3e928ee35bbc.png" class="center" width="320" height="640" >}}

形式がPNGになっていることを確認したら、 **すべてを書き出し** で書き出します。

### 4. Mastodonで絵文字登録
ユーザー設定 -> 管理 -> カスタム絵文字 で **ローカル** を選択します。

すると、 **アップロード** というボタンがあるので、クリックします。

適切に入力し、 **アップロード** ボタンを押して、アップロードします。

{{< figure src="https://s3-ap-northeast-1.amazonaws.com/mstdn.mimikun.jp/media_attachments/files/000/124/426/original/1b6dcdf05adcb6bf.png" class="center" width="320" height="640" >}}

記事は以上です。何かの参考になれば嬉しいです。

## 参考にしたページ

https://helpx.adobe.com/jp/photoshop/kb/cpsid_90772.html#anc_c
