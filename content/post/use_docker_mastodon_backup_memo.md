+++
title = "Mastodonのバックアップ(Docker環境)メモ"
date = "2018-10-08T22:29:03+09:00"
draft = false
description = ""
tags = []
categories = []
images = []
banner = ""
menu = ""
+++

**Docker環境です**

# PostgreSQL(DB) と Redis のバックアップ
## PostgreSQL(DB) のバックアップ
コンテナ止めなくてもいけるっぽい

```bash
$ sudo docker exec -it mastodon_db_1 /bin/bash     # コンテナに入る
$ su - postgres     # postgresユーザに切り替え
$ pg_dump -Fc -U postgres postgres > mastodon_docker.dump
$ exit
$ exit
```

`docker cp` でbackupファイル入れるところへぶち込む

```bash
$ sudo docker cp mastodon_db_1:/var/lib/postgresql/mastodon_docker.dump ~/backup
```

## Redis のバックアップ

```bash
$ sudo docker exec -it mastodon_redis_1 /bin/sh
$ mv dump.rdb dump.rdb.old # dump.rdb が存在する場合
$ redis-cli save
$ exit
```

`docker cp` でbackupファイル入れるところへぶち込む

```bash
$ sudo docker cp mastodon_redis_1:/data/dump.rdb ~/backup
```
